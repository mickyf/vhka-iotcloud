package ch.vhka.iotcloud;

import com.microsoft.eventhubs.client.*;

/**
 * Created by mf on 07.06.2016.
 */
public class MessageReceiver implements Runnable {

    private static EventHubClient client;
    private static long now = System.currentTimeMillis();
    private static MessageReceiver mr0;
    private static MessageReceiver mr1;

    public static boolean isRunning() {
        return mr0.isRunning || mr1.isRunning;
    }

    public static void startReceive(String connectionString, String eventHubName) {

        try {
            client = EventHubClient.create(connectionString, eventHubName);
        } catch (EventHubException e) {
            System.out.println("Exception: " + e.getMessage());
        }
        if (mr0 == null) mr0 = new MessageReceiver("0");
        if (mr1 == null) mr1 = new MessageReceiver("1");
        if (!mr0.isRunning) {
            Thread t0 = new Thread(mr0);
            t0.start();
        }
        if (!mr1.isRunning) {
            Thread t1 = new Thread(mr1);
            t1.start();
        }
    }

    public static void stopReceive() {
        mr0.stopThread = true;
        mr1.stopThread = true;
        client.close();
    }

    public boolean isRunning = false;
    public volatile boolean stopThread = false;
    private String partitionId;

    public MessageReceiver(String partitionId) {
        this.partitionId = partitionId;
    }

    public void run() {
        try {
            EventHubReceiver receiver = client.getConsumerGroup(null).createReceiver(partitionId, new EventHubEnqueueTimeFilter(now), Constants.DefaultAmqpCredits);
            System.out.println("** Created receiver on partition " + partitionId);
            while (!stopThread) {
                isRunning = true;
                EventHubMessage message = EventHubMessage.parseAmqpMessage(receiver.receive(5000));
                if (message != null) {
                    System.out.println("Received: (" + message.getOffset() + " | "
                            + message.getSequence() + " | " + message.getEnqueuedTimestamp()
                            + ") => " + message.getDataAsString());
                }
            }
            receiver.close();
            isRunning = false;
        } catch (EventHubException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}
