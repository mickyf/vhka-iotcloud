package ch.vhka.iotcloud;

/**
 * Iot Cloud Connector for VHKA
 */

import java.io.IOException;
import java.net.URISyntaxException;

import com.microsoft.eventhubs.client.Constants;
import com.microsoft.eventhubs.client.EventHubClient;
import com.microsoft.eventhubs.client.EventHubEnqueueTimeFilter;
import com.microsoft.eventhubs.client.EventHubException;
import com.microsoft.eventhubs.client.EventHubMessage;
import com.microsoft.eventhubs.client.EventHubReceiver;
import com.microsoft.eventhubs.client.ConnectionStringBuilder;

import com.microsoft.azure.iothub.DeviceClient;
import com.microsoft.azure.iothub.IotHubClientProtocol;
import com.microsoft.azure.iothub.Message;
import com.microsoft.azure.iothub.IotHubStatusCode;
import com.microsoft.azure.iothub.IotHubEventCallback;

import java.util.Random;
import java.util.Scanner;

import com.google.gson.Gson;

public class App {
    private static final String sharedAccessKey = "fUshe1KzyPE++gp6pmmc1WUQyY0UzTw/ozTqdIwZ7/s=";
    private static final String sharedAccessKeyName = "iothubowner";
    private static final String hostname = "VHKA1";
    private static final String namespace = "ihsuproddbres045dednamespace";
    private static final String eventHubName = "iothub-ehub-vhka1-41850-1a724d357b";
    private static final String connectionString = "HostName=" + hostname + ".azure-devices.net;SharedAccessKeyName=" + sharedAccessKeyName + ";SharedAccessKey=" + sharedAccessKey;

    public static void main(String[] args) throws Exception {
        // 3 functions: create device identity, send messages, receive messages
        System.out.println("Welcome to the iot cloud connector for vhka!");
        System.out.println("What do you want to do?");
        System.out.println("1 - create device identity");
        System.out.println("2 - start/stop message receiver");
        System.out.println("3 - send a message");
        System.out.println("9 - quit");
        Scanner scanner = new Scanner(System.in);
        Integer cmd = scanner.nextInt();
        while (cmd < 9) {
            switch (cmd) {
                case 1:
                    createDeviceIdentity();
                    break;
                case 2:
                    receiveMessages();
                    break;
                case 3:
                    sendMessages();
                    break;
                default:
                    System.out.println("No valid command recognized!");
                    break;
            }
            cmd = scanner.nextInt();
        }
    }

    private static void createDeviceIdentity() throws Exception {
        System.out.println("create device identity");
        System.out.println("enter identity:");
        Scanner scanner = new Scanner(System.in);
        String identity = scanner.next();
        if (!identity.isEmpty()) {
            DeviceRegistry device = new DeviceRegistry(connectionString, identity);
            System.out.println("identity: " + device.toString());
        }
    }


    private static class TelemetryDataPoint {
        public String deviceId;
        public double windSpeed;

        public String serialize() {
            Gson gson = new Gson();
            return gson.toJson(this);
        }
    }

    private static class EventCallback implements IotHubEventCallback {
        public void execute(IotHubStatusCode status, Object context) {
            System.out.println("IoT Hub responded to message with status " + status.name());

            if (context != null) {
                synchronized (context) {
                    context.notify();
                }
            }
        }
    }

    private static class MessageSender implements Runnable {
        private static String connString = "HostName={youriothubname}.azure-devices.net;DeviceId={yourdeviceid};SharedAccessKey={yourdevicekey}";
        private static IotHubClientProtocol protocol = IotHubClientProtocol.AMQPS;
        private static boolean stopThread = false;

        public void run() {
            try {
                double avgWindSpeed = 10; // m/s
                Random rand = new Random();
                DeviceClient client;
                client = new DeviceClient(connString, protocol);
                client.open();

                while (!stopThread) {
                    double currentWindSpeed = avgWindSpeed + rand.nextDouble() * 4 - 2;
                    TelemetryDataPoint telemetryDataPoint = new TelemetryDataPoint();
                    telemetryDataPoint.deviceId = "myFirstDevice";
                    telemetryDataPoint.windSpeed = currentWindSpeed;

                    String msgStr = telemetryDataPoint.serialize();
                    Message msg = new Message(msgStr);
                    System.out.println(msgStr);

                    Object lockobj = new Object();
                    EventCallback callback = new EventCallback();
                    client.sendEventAsync(msg, callback, lockobj);

                    synchronized (lockobj) {
                        lockobj.wait();
                    }
                    Thread.sleep(1000);
                }
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void sendMessages() throws IOException, URISyntaxException {
        MessageSender ms0 = new MessageSender();
        Thread t0 = new Thread(ms0);
        t0.start();

        System.out.println("Press ENTER to exit.");
        System.in.read();
        MessageSender.stopThread = true;
    }

    private static void receiveMessages() throws IOException {
        if (MessageReceiver.isRunning()) {
            MessageReceiver.stopReceive();
        } else {
            ConnectionStringBuilder csb = new ConnectionStringBuilder(sharedAccessKeyName, sharedAccessKey, namespace);
            MessageReceiver.startReceive(csb.getConnectionString(), eventHubName);
        }
    }


}
