package ch.vhka.iotcloud;

import com.microsoft.azure.iot.service.exceptions.IotHubException;
import com.microsoft.azure.iot.service.sdk.Device;
import com.microsoft.azure.iot.service.sdk.DeviceStatus;
import com.microsoft.azure.iot.service.sdk.RegistryManager;

/**
 * Created by mf on 07.06.2016.
 */
public class DeviceRegistry {

    private RegistryManager registryManager;
    private Device device;

    @Override
    public String toString() {
        return "Device id: " + device.getDeviceId() + "; Device key: " + device.getPrimaryKey();
    }

    public DeviceRegistry(String connectionString, String deviceId) throws Exception {
        registryManager = RegistryManager.createFromConnectionString(connectionString);

        device = Device.createFromId(deviceId, DeviceStatus.Disabled, null);
        try {
            device = registryManager.addDevice(device);
        } catch (IotHubException iote) {
            try {
                device = registryManager.getDevice(deviceId);
            } catch (IotHubException iotf) {
                iotf.printStackTrace();
            }
        }

    }
}
